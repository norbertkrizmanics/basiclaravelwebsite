<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['middleware' =>['web']],function (){

    //login and register
Route::post('login',['uses'=>'Auth\LoginController@login','as'=>'login']);
Route::get('logout',['uses'=>'Auth\LoginController@logout','as'=>'logout']);
Route::post('register',['uses'=>'Auth\RegisterController@register','as'=>'register']);

//send reset password to email
Route::post('password.email','Auth\ForgotPasswordController@sendResetLinkEmail');
Route::post('password.reset','Auth\ResetPasswordController@reset');
Route::get('password.reset','Auth\ForgotPasswordController@showLinkRequestForm');
Route::get('password.reset.{token}','Auth\ResetPasswordController@showResetForm');

//categories
Route::resource('categories','CategoryController',['except'=>'create']);
//tags
Route::resource('tags','TagController',['except'=>'create']);

//comments
Route::post('comments/{id}',['uses'=>'CommentsController@store','as'=>'comments.store']);
Route::get('comments/{id}/edit',['uses'=>'CommentsController@edit','as'=>'comments.edit']);
Route::put('comments/{id}',['uses'=>'CommentsController@update','as'=>'comments.update']);
Route::delete('comments/{id}',['uses'=>'CommentsController@destroy','as'=>'comments.destroy']);
Route::get('comments/{id}/delete',['uses'=>'CommentsController@delete','as'=>'comments.delete']);

Route::get('blog/{slug}',['as'=>'blog.single','uses'=>'BlogController@getSingle'])->where('slug','[\w\d\-\_]+');
Route::get('blogs',['uses'=>'BlogController@getIndex', 'as'=>'blogs.index']);

Route::get('/','PagesController@getHome');
Route::get('/about','PagesController@getAbout');
Route::get('/blog','PagesController@getBlog');
Route::resource('posts','PostController');
Route::get('/contact',['uses'=>'PagesController@getContact','as'=>'contact']);
Route::post('contact','PagesController@postContact');

Route::get('/home', 'HomeController@index')->name('home');
Route::post('contact/submit','MessagesController@submit');
Route::get('messages',['uses'=>'MessagesController@getMessages','as'=>'messages']);
});