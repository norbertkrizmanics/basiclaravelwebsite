<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Post;
use Mail;
use Session;

class PagesController extends Controller
{
    //Route View Controller
    public function getHome(){
        return view('home');
    }
    public function getAbout(){
        return view('about');
    }
    public function getBlog(){
        $posts=Post::orderBy('created_at','desc')->limit(10)->get();

        return view('blog')->withPosts($posts);
}
    public function getContact(){
        return view('contact');
    }

    public function postContact(Request $request){
    $this->validate($request,[
        'name'=>'required',
        'email'=>'required',
        'message'=>'min:10'
    ]);

    $data = [
        'name'=>$request->name,
        'email'=>$request->email,
        'bodyMessage'=>$request->message,

    ];
    Mail::send('emails.contact', $data, function ($message) use($data){

        $message->from($data['email']);
        $message->to('krizmanics.norbert@gmail.com');

    });

    Session::flash('success','Your email was sent.');
    return redirect()->route('contact');
    }
}
