@extends('layouts.app')
@section('Title','| DELETE COMMENTS')
@section('content')
    <h1>Delete this comment?</h1>
    <p>
        <strong>Name:</strong> {{$comment->name}}<br>
        <strong>Email:</strong> {{$comment->email}}<br>
        <strong>Comment:</strong> {{$comment->comment}}<br>
    </p>
    {{Form::open(['route'=>['comments.destroy',$comment->id],'method'=>'DELETE'])}}
    {{Form::submit('Yes, delete this comment!',['class'=>'btn btn-danger btn-block btn-lg'])}}
    {{Form::close()}}
@endsection