@extends('layouts.app')
@section('title','| Edit content')
@section('content')
    <h1>Edit</h1>
    {{Form::model($comment,['route'=>['comments.update',$comment->id], 'method'=>'PUT'])}}
    {{Form::label('name','Name: ')}}
    {{Form::text('name',null,['class'=>'form-control','disabled'=>''])}}
    {{Form::label('email','Email:')}}
    {{Form::text('email',null,['class'=>'form-control','disabled'=>''])}}
    {{Form::label('comment','Comment:')}}
    {{Form::textarea('comment',null,['class'=>'form-control'])}}
    {{Form::submit('Update comment',['class'=>'btn btn-success btn-block'])}}
    {{Form::close()}}
@endsection