@extends('layouts.app')
@section('title', "$tag->name Tag")
@section('content')
    <h1> {{$tag->name }} Tag <small>{{$tag->posts()->count()}} Posts</small></h1>
    <div class="row">
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Title</th>
            <th>Tags</th>
            <th>View</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tag->posts as $post)
        <tr>
        <th>{{$post->id}}</th>
            <td>{{$post->title}}</td>
            <td>@foreach($post->tags as $tag)

                <span class="btn btn-secondary btn-sm">{{$tag->name}}</span>
                @endforeach
            </td>
            <td><a href="{{route('posts.show',$post->id)}}" class="btn btn-success btn-sm">View</a></td>
        </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    @endsection
@section('sidebar')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Tags:</h5>
            <p class="card-text">Editing</p>
            <dl class="list-group-item">
                <dt>Tags:</dt>
                <dd><a href="{{route('tags.edit',$tag->id)}}" class="btn btn-primary btn-block">Edit</a></dd>
                <hr>
                <dd>{{Form::open(['route'=>['tags.destroy',$tag->id],'method'=>'DELETE'])}}
                    {{Form::submit('Delete',['class'=>'btn btn-danger btn-block'])}}
                    {{Form::close()}}
                </dd>
            </dl>



        </div>
    </div>
@endsection