@extends('layouts.app')
@section('title','| All Tags')
@section('content')
    <h1>Tags</h1>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>

        </tr>
        </thead>
        <tbody>
        @foreach($tags as $tag)
            <tr>
                <th>{{$tag->id}}</th>
                <td><a href="{{route('tags.show',$tag->id)}}">{{$tag->name}} </a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
@section('sidebar')
    {{Form::open(['route'=>'tags.store','methos'=>'POST'])}}

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">New Tag:</h5>
            <p class="card-text">Create</p>
            <hr>
            <dl class="list-group-item">
                <dt> {{Form::label('name','Name: ')}}</dt>
                <dd>{{Form::text('name',null,['class'=>'form-control'])}}</dd>
                {{Form::submit('Create new tag',['class'=>'btn btn-primary btn-block'])}}
            </dl>
            <hr>



        </div>
    </div>

    {{Form::close()}}
@endsection
@section('body')
    <hr>
@endsection