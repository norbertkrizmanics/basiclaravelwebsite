@extends('layouts.app')
@section('title', '| Contact')

@section('content')
    <h1 class="animated bounce">Contact</h1>

    <form action="{{url('contact')}}" method="POST">
        {{csrf_field()}}
    {!! Form::open(['url'=>'contact/submit','data-parsley-validate'=>'']) !!}
    <div class="form-group">
         {{Form::label('name', 'Name')}}
        {{Form::text('name', '',['class'=>'form-control','placeholder'=>'Enter name', 'required'=>''])}}
    </div>
    <div class="form-group">
         {{Form::label('email', 'E-Mail Address')}}
        {{Form::text('email', '',['class'=>'form-control','placeholder'=>'Enter email', 'required'=>''])}}
    </div>
    <div class="form-group">
         {{Form::label('message', 'Message')}}
        {{Form::textarea('message', '',['class'=>'form-control','placeholder'=>'Enter message', 'required'=>''])}}
    </div>
    <div>
        {{Form::submit('Submit',['class'=>'btn btn-success'])}}
    </div>
    {!! Form::close() !!}
    </form>

@endsection