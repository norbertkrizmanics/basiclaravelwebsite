
<div class="container">
    @if(Request::is('/'))
        @include('includes.showcase')
    @endif
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-lg-8">
                @include('includes.messages')
                @yield('content')
            </div>
            <div class="col-xs-12 col-sm-4 col-lg-4">
                @include('includes.sidebar')
            </div>
        </div>
    </div>
</div>
<div class="container text-center">
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            @yield('body')
        </div>
    </div>
</div>

