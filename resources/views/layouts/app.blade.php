<!doctype html>
<html lang="en">
<head>
   @include('includes.head')
    @section('stylesheet')
        {!! Html::style('css/parsley.css') !!}
        {!! Html::style('css/styles.css') !!}
        {!! Html::style('css/select2.min.css') !!}
        @endsection
</head>
<body>

@include('includes.navbar')

@include('includes.body')

@include('includes.footer')

@include('includes.scripts')

</body>
</html>