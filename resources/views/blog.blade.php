@extends('layouts.app')
@section('title', '| Blog')

@section('content')
    @foreach($posts as $post)
        <div class="post">
    <h1>{{$post->title}}</h1>
    <p>{{substr(strip_tags($post->body),0,300)}}{{strlen(strip_tags($post->body))> 300 ? '...' : ''}}</p>
        <a href="{{url('blog/'.$post->slug)}}" class="btn btn-success animated pulse">Read More</a>
            <hr>
    </div>
    @endforeach

        @endsection
@section('sidebar')
    @parent
    <p>This is appended to the sidebar</p>
@endsection
