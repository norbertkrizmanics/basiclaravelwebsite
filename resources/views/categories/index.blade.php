@extends('layouts.app')
@section('title','| All Categories')
@section('content')
    <h1>Categories</h1>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
        <tr>
            <th>{{$category->id}}</th>
            <td>{{$category->name}}</td>
        </tr>
            @endforeach
        </tbody>
    </table>
@endsection
@section('sidebar')
    {{Form::open(['route'=>'categories.store','methos'=>'POST'])}}

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">New category:</h5>
            <p class="card-text">Create</p>
            <hr>
            <dl class="list-group-item">
                <dt> {{Form::label('name','Name: ')}}</dt>
                <dd>{{Form::text('name',null,['class'=>'form-control'])}}</dd>
                {{Form::submit('Create new category',['class'=>'btn btn-primary btn-block'])}}
            </dl>
            <hr>



        </div>
    </div>

    {{Form::close()}}
@endsection
@section('body')
    <hr>
    @endsection