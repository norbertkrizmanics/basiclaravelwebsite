@extends('layouts.app')
@section('title' ,'|Blog')
@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-12">
            <h1>Blog</h1>
        </div>
    </div>
    @foreach($posts as $post)
    <div class="row">
        <div class="col-xs-12 col-sm-8">
            <h2>{{$post->Title}}</h2>
            <h5>Published: {{date('Y F j , H:i',strtotime($post->created_at))}}</h5>
            <p>{{ substr(strip_tags($post->body),0,250)}} {{strlen(strip_tags($post->body))>250 ? '...' : ''}}</p>
            <a href="{{route('blogs.index',$post->id)}}">Read More</a>
            <hr>
        </div>
    </div>
    <div class="pagination justify-content-center">
        {!! $posts->links(); !!}
    </div>
    @endforeach
@endsection