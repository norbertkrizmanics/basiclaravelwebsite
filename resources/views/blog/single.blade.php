@extends('layouts.app')
<?php $titleTag = htmlspecialchars($post->title); ?>
@section('title', "| $titleTag")
@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-offset-2">
            <img src="{{asset('images/'.$post->image)}}" class="img-fluid rounded mx-auto" alt="">
        <h1>{{$post->title}}</h1>
            <p>{!! $post->body !!}</p>
            <hr>
            <p>Category: {{$post->category->name}}</p>
            <hr>
            <div class="tags">
                @foreach($post->tags as $tag)
                    <span class="btn btn-secondary btn-sm">{{$tag->name}}</span>
                @endforeach
            </div>
        </div>
    </div>
@endsection
@section('sidebar')
    <div class="card card-body bg-light">
        <h3 class="comments-title"><span class="glyphicon glyphicon-comment"></span>{{$post->comments()->count()}} Comments</h3>
        @foreach($post->comments as $comment)
            <div class="comment">
            <div class="author-info">
                <img src="{{"https://www.gravatar.com/avatar/".md5(strtolower(trim($comment->email)))."?s=50&d=mm"}}" class="author-image" alt="">
                <div class="author-name">
                    <h4>Name: {{$comment->name}} </h4>
                    <div class="author-time">
                    <p>Name: {{$comment->created_at}} </p>
                    </div>
                </div>
            </div>
                <div class="comment-content">
                    <p>Comment:{{$comment->comment}} </p>

                </div>
            </div>
        @endforeach
    </div>
@endsection
@section('body')


    <div id="comment-form" class="col-md-12 col-md-offset-2">
    {{Form::open(['route'=>['comments.store',$post->id],'method'=>'POST'])}}
    {{Form::label('name','Name:')}}
    {{Form::text('name',null,['class'=>'form-control'])}}

        {{Form::label('email','Email:')}}
    {{Form::text('email',null,['class'=>'form-control'])}}
    {{Form::label('comment','Comment: ')}}
    {{Form::textarea('comment',null,['class'=>'form-control'])}}
    {{Form::submit('Add comment',['class'=>'btn btn-success btn-block'])}}
    {{Form::close()}}
    </div>
@endsection