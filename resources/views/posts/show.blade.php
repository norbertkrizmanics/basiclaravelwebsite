@extends('layouts.app')
@section('title', '| Show')
@section('content')
    <img class="img-fluid" src="{{asset('images/'.$post->image)}}" alt="Photo">
    <h1>{{$post->title}}</h1>
    <p class="lead">{!! $post->body !!}</p>
    <hr>
    <div class="tags">
    @foreach($post->tags as $tag)
    <span class="btn btn-secondary btn-sm">{{$tag->name}}</span>
    @endforeach
    </div>
    <div class="backend-comments">
        <h3>Comments <small>{{$post->comments()->count()}} total </small></h3>
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Comment</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($post->comments as $comment)
            <tr>
                <td>{{$comment->name}}</td>
                <td>{{$comment->email}}</td>
                <td>{{$comment->comment}}</td>
                <td><a href="{{route('comments.edit',$comment->id)}}" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-pencil"></span>Edit</a></td>
                <td><a href="{{route('comments.delete',$comment->id)}}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash">Delete</span></a></td>
            </tr>
             @endforeach
            </tbody>
        </table>

    </div>
    @endsection
@section('sidebar')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Blog Post</h5>
            <p class="card-text">Modifications</p>
            <hr>
            <dl class="list-group-item">
                <dt>Url slug:</dt>
                <dd><a href="{{ route('blog.single',$post->slug) }}">{{ route('blog.single',$post->slug) }}</a></dd>
            </dl>
            <hr>
            <dl class="list-group-item">
                <dt>Category:</dt>
                <dd>{{$post->category->name}}</dd>
            </dl>
            <hr>
            <dl class="list-group-item">
                <dt>Created At:</dt>
                <dd> {{date('Y F j , H:i',strtotime($post->created_at))}}</dd>
            </dl>
            <hr>
            <dl class="list-group-item">
                <dt>Last Updated:</dt>
                <dd>{{date('Y F j , H:i',strtotime($post->updated_at ))}}</dd>
            </dl>
            <hr>

            <div class="row">


                <div class="col-xs-6 col-sm-6 col-md-6">
                    {!! Html::linkRoute('posts.edit','Edit',[$post->id],['class'=>'btn btn-primary btn-inline btn-block']) !!}
                    <hr>
                </div>

                <div class="col-xs-6 col-sm-6 col-md-6">
                    {!! Form::open(['route' =>['posts.destroy',$post->id],'method'=>'DELETE']) !!}
                    {!! Form::submit('Delete',['class'=>'btn btn-danger btn-block']) !!}
                    {!! Form::close() !!}
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    {{Html::linkRoute('posts.index','<<< See All Posts', [],['class'=>'btn btn-default btn-block btn-h1-spacing'])}}
                </div>
            </div>
        </div>
    </div>
    @endsection