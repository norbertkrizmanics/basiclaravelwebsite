
@extends('layouts.app')
@section('title', '| Index')
@section('content')
    <div class="row">
        <h1>All Blog posts</h1>
        </div>
@endsection
@section('sidebar')
    <div class="card">
        <div class="card-body">
        <h5 class="card-title">Create: </h5>
            <p class="card-text">New blog posts</p>
        <a href="{{route('posts.create')}}" class="btn btn-primary btn-block btn-lg">Create new blog post</a>
        </div>
    </div>
    @endsection
@section('body')

    <table class="table">
        <thead>
        <th>#</th>
        <th>Title</th>
        <th>Body</th>
        <th>Created At</th>
        <th>View</th>
        <th>Edit</th>

        </thead>

        <tbody>
            @foreach($posts as $post)
            <tr>
                <th>{{ $post->id }}</th>
                <td>{{ $post->title }}</td>
                <td>{{ substr(strip_tags($post->body),0,50) }} {{strlen(strip_tags($post->body))>50 ? '...' : ''}}</td>
                <td>{{ date('Y F j , H:i',strtotime($post->created_at)) }}</td>
                <td><a href="{{route('posts.show',$post->id)}}" class="btn btn-info btn-block btn-sm">View</a></td>
                 <td><a href="{{route('posts.edit',$post->id)}}" class="btn btn-info btn-block btn-sm">Edit</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pagination justify-content-center">
    {!! $posts->links(); !!}
    </div>

@endsection
