@extends('layouts.app')
@section('title','| Edit Blog Post ')
@section('stylesheet')
    <script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>


    <script>
        tinymce.init({
            selector:'textarea',
            plugins:'link code',
        });
    </script>
@section('content')
    {!! Form::model($post,['route'=>['posts.update',$post->id],'method'=>'PUT','files'=>true]) !!}
    {{Form::label('title','Title:')}}
    {{Form::text('title',null,['class'=>'form-control'])}}
    {{Form::label('slug','Slug:')}}
    {{Form::text('slug',null,['class'=>'form-control','placeholder'=>'slug-text-like-this'])}}
    {{Form::label('category_id','Category:')}}
    {{Form::select('category_id',$categories,null,['class'=>'form-control'])}}

    {{Form::label('tags','Tags: ')}}
    {{Form::select('tags[]',$tags,null,['class'=>'form-control select2-multi','multiple'=>'multiple'])}}

    {{Form::label('featured_image','Update featured image')}}
    {{Form::file('featured_image')}}

    {{Form::label('body','Body: ')}}
    {{Form::textarea('body',null,['class'=>'form-control'])}}
    <hr>
@endsection
@section('sidebar')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Blog Post</h5>
            <p class="card-text">Editing</p>
            <hr>
            <dl class="list-group-item">
                <dt>Url slug:</dt>
                <dd><a href="{{url($post->slug)}}">{{url($post->slug)}}</a></dd>
            </dl>
            <hr>
            <dl class="list-group-item">
                <dt>Created At:</dt>
                <dd> {{date('Y F j , H:i',strtotime($post->created_at))}}</dd>
            </dl>
            <hr>
            <dl class="list-group-item">
                <dt>Last Updated:</dt>
                <dd>{{date('Y F j , H:i',strtotime($post->updated_at ))}}</dd>
            </dl>
            <hr>

            <div class="row">


                <div class="col-xs-6 col-sm-6 col-md-6">
                    {!! Html::linkRoute('posts.show','Cancel',[$post->id],['class'=>'btn btn-danger btn-block']) !!}

                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    {{Form::submit('Save',['class'=>'btn btn-success btn-block'])}}

                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection


@section('scripts')
    {!! Html::script('js/select2.min.js') !!}
    <script type="text/javascript">
        $('.select2-multi').select2();
        $('.select2-multi').select2().val({!! json_encode($post->tags()->allRelatedIds()) !!}).trigger('change');
    </script>
@endsection