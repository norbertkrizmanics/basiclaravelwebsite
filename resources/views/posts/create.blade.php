@extends('layouts.app')
@section('title', '| Create')
@section('stylesheet')
    <script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>


    <script>
    tinymce.init({
        selector:'textarea',
        plugins:'link code',
    });
</script>
@endsection
@section('content')
    <h1>Create new post</h1>
    <hr>

    {!! Form::open(['route' => 'posts.store','data-parsley-validate'=>'','files'=>true]) !!}
     {{Form::label('title','Title: ')}}
     {{Form::text('title', null, ['class'=>'form-control','required'=>''])}}

    {{Form::label('slug','Slug:')}}
    {{Form::text('slug',null,['class'=>'form-control','required'=>'','minlength'=>'5','placeholder'=>'slug-text-like-this'])}}

    {{Form::label('category_id','Category: ')}}
    <select name="category_id" class="form-control select2-multi" multiple="multiple" id="category_id">
        @foreach($categories as $category)
        <option value="{{$category->id}}"> {{$category->name}}</option>
        @endforeach
    </select>

    {{Form::label('tags','Tags: ')}}
    <select name="tags" class="form-control select2-multi" multiple="multiple" id="tags">
        @foreach($tags as $tag)
        <option value="{{$tag->id}}"> {{$tag->name}}</option>
        @endforeach
    </select>
    <hr>
    {{Form::label('featured_image','Upload featured image:')}}
    {{Form::file('featured_image')}}
    <hr>
    {{Form::label('body', 'Post Body: ')}}
    {{Form::textarea('body',null,['class'=>'form-control'])}}
    {{Form::submit('Create Post',['class' => 'btn btn-success, btn-block','style'=>'margin-top:20px;'])}}
    {!! Form::close() !!}
    @endsection

@section('scripts')
     {!! Html::script('js/select2.min.js') !!}
     <script type="text/javascript">
         $('.select2-multi').select2();
     </script>
    @endsection